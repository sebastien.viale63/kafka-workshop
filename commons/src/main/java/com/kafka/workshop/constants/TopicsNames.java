package com.kafka.workshop.constants;


/**
 * Contains all the names of the topics
 */
public class TopicsNames {
    
    public final static String PREFIX = "MY_PREFIX_";

    public final static String SIMPLE_TOPIC = PREFIX + "simple-topic";

    public final static String SASL_SIMPLE_TOPIC = PREFIX + "sasl-simple-topic";

    public final static String AVRO_SIMPLE_TOPIC = PREFIX + "avro-simple-topic";

    public final static String AVRO_SERIALIZATION_ERROR_TOPIC = PREFIX + "avro-serialization-topic";

    public final static String RETRIABLE_TOPIC = PREFIX + "retriable-topic";

    public final static String EXACTLY_ONCE_TOPIC = PREFIX + "exactly-once-topic";
    
    public final static String SIMPLE_STREAM_IN = PREFIX + "simple-stream-in";

    public final static String SIMPLE_STREAM_OUT = PREFIX + "simple-stream-out";

    public final static String TRANSFORM_STREAM_IN = PREFIX + "transform-stream-in";

    public final static String TRANSFORM_STREAM_OUT = PREFIX + "transform-stream-out";

    public final static String TRANSFORM_TABLE_IN = PREFIX + "transform-stream-in";

    public final static String TRANSFORM_TABLE_OUT = PREFIX + "transform-stream-out";

    public final static String JOIN_STREAM_LEFT_IN = PREFIX + "join-stream-left-in";

    public final static String JOIN_STREAM_RIGHT_IN = PREFIX + "join-stream-right-in";
    
    public final static String JOIN_TABLE_IN = PREFIX + "join-table-in";
    
    public final static String JOIN_STREAM_TABLE_OUT = PREFIX + "join-stream-table-out";

    public final static String JOIN_STREAM_STREAM_OUT = PREFIX + "join-stream-stream-out";

    public final static String WINDOWS_STREAM_IN = PREFIX + "windows-stream-in";

    public final static String WINDOWS_STREAM_COUNT_OUT = PREFIX + "windows-stream-count-out";

    public final static String WINDOWS_STREAM_SUM_OUT = PREFIX + "windows-stream-sum-out";

    public final static String SPLIT_STREAM_IN = PREFIX + "split-topic-in";

    public final static String SPLIT_VALID_STREAM_IN = PREFIX + "split-valid-topic-in";

    public final static String SPLIT_VALID_STREAM_OUT = PREFIX + "split-valid-topic-out";

    public final static String SPLIT_INVALID_STREAM_OUT = PREFIX + "split-invalid-topic-out";

    public final static String DEDUPLICATE_TOPIC_IN = PREFIX + "deduplicate-topic-in";
    
    public final static String DEDUPLICATE_TOPIC_OUT = PREFIX + "deduplicate-topic-out";

    public final static String INT_QUERY_TOPIC = "int-query-topic";

    public final static String ERROR_HANDLER_TOPIC_IN = "handler-topic-in";

    public final static String ERROR_HANDLER_TOPIC_OUT = "handler-topic-out";




}
