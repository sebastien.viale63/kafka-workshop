package com.kafka.workshop.constants;

public class KafkaProperties {
    
    public static final String BOOTSTRAP_SERVERS = "localhost:19092,localhost:29092,localhost:39092";

    public static final String SCHEMA_REGISTRY = "http://localhost:8081";
    
    public static final String STORE_DIR = "D://temp//streams";
}
