package solution.com.kafka.workshop.producer;

import com.kafka.workshop.avro.*;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AvroProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(AvroProducer.class);

    // TODO: Set the topic name
    private String topic = TopicsNames.AVRO_SIMPLE_TOPIC;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Topics.createTopics(topic);

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);

        // TODO: Set the Schema Registry url
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // TODO: Set the key and value serializers KafkaAvroSerializer
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 3);

        // TODO: Create an Producer instance with WorkshopKey key and WorkshopValue key
        Producer<WorkshopKey, WorkshopValue> producer = new KafkaProducer<>(props);

        try {

            for(int i=0; i<10; i++) {
                WorkshopKey workshopKey = WorkshopKey.newBuilder()
                        .setKeyField1(getAlphaNumericString(3))
                        .setKeyField2(getAlphaNumericString(3))
                        .build();

                WorkshopN workshopN = WorkshopN.newBuilder()
                        .setNestedField(getAlphaNumericString(5))
                        //  .setNestedField2(getAlphaNumericString(5))
                        .build();
                WorkshopL workshopL = WorkshopL.newBuilder()
                        .setListWorkshopField(getAlphaNumericString(10))
                        .build();
                List<WorkshopL> list = new ArrayList<>();
                list.add(workshopL);

                WorkshopM workshopM = WorkshopM.newBuilder()
                        .setMapWorkshopField(getAlphaNumericString(10))
                        .build();
                Map<String, WorkshopM> map = new HashMap<>();
                map.put(getAlphaNumericString(3), workshopM);

                WorkshopValue workshopValue = WorkshopValue.newBuilder()
                        .setValueField1(getAlphaNumericString(4))
                        .setValueField2(getInteger())
                        .setValueField3(getAlphaNumericString(5))
                        //.setValueField4(getAlphaNumericString(5))
                        .setListWorkshop(list)
                        .setMapWorkshop(map)
                        .setNestedWorkshop(workshopN)
                        .build();

                // TODO: Send record
                ProducerRecord<WorkshopKey, WorkshopValue> avroRecord = new ProducerRecord<>(topic, workshopKey, workshopValue);
                producer.send(avroRecord, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error(e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                }).get();
            }

        } finally {
            producer.close();
        }

    }

    private String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    private Integer getInteger() {
        int number = (int) (Math.random() * 1000);
        return number;
    }





}
