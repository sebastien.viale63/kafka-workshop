package solution.com.kafka.workshop.consumer;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class ExactlyOnceConsumer {

    public static void main(String[] args) {
        ExactlyOnceConsumer exactlyOnceConsumer = new ExactlyOnceConsumer();
        exactlyOnceConsumer.start();
    }

    private static final String TOPIC_NAME = TopicsNames.EXACTLY_ONCE_TOPIC;
    private final Logger logger = LoggerFactory.getLogger(ExactlyOnceConsumer.class);
    private final Properties properties = new Properties();

    public ExactlyOnceConsumer() {
        buildCommonProperties();
    }
    public void start() {
       

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
        logger.info("Subscribing to `{}` topic", TOPIC_NAME);
        consumer.subscribe(Collections.singleton(TOPIC_NAME));

        while (consumer.assignment().size() == 0) {
            consumer.poll(Duration.ofMillis(0));
        }
        consumer.seekToBeginning(consumer.assignment());

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
            for (ConsumerRecord<String, String> record : records) {
                logger.info("Received offset = {}, partition = {}, key = {}, value = {}", record.offset(), record.partition(), record.key(), record.value());
            }
            if (!records.isEmpty()) {
                consumer.commitSync();
            }
        }
    }

    private Properties buildCommonProperties() {
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, TopicsNames.PREFIX + "groupid");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        properties.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        return properties;
    }

}
