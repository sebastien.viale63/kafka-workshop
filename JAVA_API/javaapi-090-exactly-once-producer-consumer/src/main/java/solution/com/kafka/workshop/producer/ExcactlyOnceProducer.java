package solution.com.kafka.workshop.producer;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ExcactlyOnceProducer {

    public static void main(String[] args) {
        ExcactlyOnceProducer simpleProducer = new ExcactlyOnceProducer();
        simpleProducer.start();
    }

    private static final String TOPIC_NAME = TopicsNames.EXACTLY_ONCE_TOPIC;
    private final Logger logger = LoggerFactory.getLogger(ExcactlyOnceProducer.class);
    private final Properties properties = new Properties();


    public ExcactlyOnceProducer() {
        buildCommonProperties();
    }

    private void start() {
        logger.info("Sending data to `{}` topic", TOPIC_NAME);
        Producer<String, String> producer = new KafkaProducer<>(properties);
            
        // prepare producer to use transactions
        producer.initTransactions();
        
        String key = "";
        String value = "";
        int cpt = 0;
        try {
            while (cpt < 10) {
                // begin transaction
                producer.beginTransaction();
                //sending 100 messages
                for (int i = 0; i < 10; i++) {
                    TimeUnit.MILLISECONDS.sleep(100);
                    key = getAlphaNumericString(5);
                    value = getAlphaNumericString(20);
                    if(cpt == 9  && i == 5){
                        throw  new Exception();
                    }
                    ProducerRecord<String, String> record = new ProducerRecord<String, String>(TOPIC_NAME, cpt + "_" + key, value);
                    producer.send(record, new Callback() {
                        public void onCompletion(RecordMetadata metadata, Exception e) {
                            if (e != null) {
                                logger.error(e.getMessage());
                                logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                            } else {
                                logger.info("KAFKA SUCCESS key={} topic={} partition={} offset={}", record.key(), metadata.topic(), metadata.partition(), metadata.offset());
                            }
                        }
                    });
                }
                cpt++;
                // commit transaction
                producer.commitTransaction();
            }
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            logger.error("aborting transaction {} ", cpt);
            // abort transaction
            producer.abortTransaction();
          
        } finally {
            producer.close();

        }        
    }

    private void buildCommonProperties() {
        properties.putIfAbsent(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        properties.putIfAbsent(ProducerConfig.ACKS_CONFIG, "all");
        properties.putIfAbsent(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.putIfAbsent(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.putIfAbsent(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true);
        properties.putIfAbsent(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "exactly_once_producer_transaction_id");
    }

    private String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

}
