package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class AvroConsumerDeserializationApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(AvroConsumerDeserializationApplication.class);
        application.run(args);
    }
	
}
