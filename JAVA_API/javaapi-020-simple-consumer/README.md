#### This exercise aims at writing a simple java KAFKA consumer

#### Start the Kafka cluster :
If your Docker containers of the previous the exercise 02_simple_producer are stopped, start them
```docker-compose start```

The cluster is composed of 1 zookeeper and 3 brokers and 1 schema-registry   
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records 

##### In the partial.com.kafka.workshop.consumer.SimpleConsumer java class:
#### Topic name:
Set the topic name you will consume records from using "topic" variable. Set it to *simpleTopic*
#### Consumer configuration :
Configure the KAFKA consumer using the org.apache.kafka.clients.consumer.ConsumerConfig class  
- the broker list using the BOOTSTRAP_SERVERS_CONFIG constant  
  for ex: *"localhost:19092,localhost:29092,localhost:39092"*)
- the consumer group id using the GROUP_ID_CONFIG constant
- the deserializer for the key and the value of the record to consume using the KEY_DESERIALIZER_CLASS_CONFIG and VALUE_DESERIALIZER_CLASS_CONFIG constants  
  We will consume some String messages: StringDeserializer.class
- from wich offset to consumr when there is no initial offset or if the current offset does not exist any more using the AUTO_OFFSET_RESET_CONFIG constant
earliest: automatically reset the offset to the earliest offset  
latest: automatically reset the offset to the latest offset  
none: throw exception to the consumer if no previous offset is found for the consumer's group  
- the autocommit strategy using the ENABLE_AUTO_COMMIT_CONFIG constant (set it to false)

#### Consuming records :
- Create a org.apache.kafka.clients.consumer.KafkaConsumer instance
- Subscribe to the topic using the subscribe method of the KafkaConsumer instance
- In the while loop :
    create a ConsumerRecords using the poll method of the KafkaConsumer instance
    iterate through the polled list of records using the forEach method of the ConsumerRecords instance
    display the key, value, offset, partition, topic and the header (if not null) of each record using the Logger
- Commit records to the cluster when each record off a poll have been treated
- Start the application...
- Start another instance of the application, note how partitions are rebalanced between consumers

#### Consuming records from beginning:
Before the while loop :
- Wait for partitions to be assigned to the consumer
  use the assignment() and poll() method of the KafkaConsumer instance. A poll(0) will only check that the consumer is ready to consume
- Reset the consumer to the first offset of the topic using the seekToBeginning(Set<TopicPartition>) method of the consumer

