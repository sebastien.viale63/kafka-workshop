package solution.com.kafka.workshop.producer;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class SimpleProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = TopicsNames.SIMPLE_TOPIC;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Topics.createTopics(topic);

        Properties props = new Properties();
        // TODO: Set the bootstrap servers
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        
        // TODO: Set acks config to all
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        //    props.put(ProducerConfig.ACKS_CONFIG, "1");
        // TODO: Set retries config to 3
        props.put(ProducerConfig.RETRIES_CONFIG, 3);
        // TODO: Set the key and value serializers
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        //TODO: set the batch size config to default value
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        //    props.put(ProducerConfig.BATCH_SIZE_CONFIG, 33554432);
        // TODO: set the linger ms config to default value 0
        props.put(ProducerConfig.LINGER_MS_CONFIG, 0);
        //  props.put(ProducerConfig.LINGER_MS_CONFIG, 1000);


        // TODO: Create an Producer instance with String key and Value key
        Producer<String, String> producer = new KafkaProducer<String, String>(props);


        String key = "";
        String value = "";
        try {
            //sending 100 messages
            for (int i = 0; i < 100; i++) {
                key = getAlphaNumericString(5);
                value = getAlphaNumericString(20);
                // TODO: Create a producer record instance with String key and Value key
                ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, key, value);

                // TODO: Add some headers (for ex: add the key)
                buildHeader(record, "record_key", key);

                boolean getCallBack = true;
                if (!getCallBack) {
                    // TODO: Send record
                    producer.send(record);
                } else {
                    // TODO: Send record to Kafka using callBack
                    producer.send(record, new Callback() {
                        public void onCompletion(RecordMetadata metadata, Exception e) {
                            if (e != null) {
                                logger.error(e.getMessage());
                                logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                            } else {
                                logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                            }
                        }
                    });
                }

            }
        } finally {
            producer.close();
        }
    }

    private void buildHeader(ProducerRecord<String, String> record, String headerKey, String headerValue) {
        // TODO: Create an instance of a record header and add the header to the record
        RecordHeader recordHeader = new RecordHeader(headerKey, headerValue.getBytes());
        record.headers().add(recordHeader);
    }

       private String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


}
