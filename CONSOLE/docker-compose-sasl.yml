version: '2.1'
services:
  zookeeper:
    image: confluentinc/cp-zookeeper:6.1.1
    hostname: zookeeper
    container_name:  zookeeper
    ports:
      - "2181:2181"
    healthcheck:
      test:  nc -z localhost 2181 || exit 1
      interval: 5s
      timeout: 5s
      retries: 30
    environment:
      ZOOKEEPER_SERVER_ID: 1
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
      ZOOKEEPER_INIT_LIMIT: 5
      ZOOKEEPER_SYNC_LIMIT: 2
      ZOOKEEPER_SERVERS: zookeeper:2888:3888
      KAFKA_OPTS: -Djava.security.auth.login.config=/etc/kafka/secrets/zookeeper_jaas.conf
    volumes:
      - ./etc/secrets:/etc/kafka/secrets
    networks:
      - workshop-network-sasl

  kafka-1:
    image: confluentinc/cp-kafka:6.1.1
    hostname: kafka-1
    container_name:  kafka-1
    ports:
      - "19092:19092"
    depends_on:
      zookeeper:
        condition: service_healthy
    healthcheck:
      test: kafka-topics --bootstrap-server localhost:9092 --list --command-config /etc/kafka/sasl/admin-sasl.properties
      interval: 5s
      timeout: 5s
      retries: 10
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: SASL_PLAINTEXT:SASL_PLAINTEXT,PLAINTEXT_HOST:SASL_PLAINTEXT,
      KAFKA_ADVERTISED_LISTENERS: SASL_PLAINTEXT://kafka-1:9092,PLAINTEXT_HOST://localhost:19092
      #     KAFKA_ADVERTISED_LISTENERS: SASL_PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
      KAFKA_DEFAULT_REPLICATION_FACTOR: 3
      KAFKA_NUM_PARTITIONS: 3
      KAFKA_MIN_INSYNC_REPLICAS: 2
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 3
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 2
      KAFKA_SECURITY_INTER_BROKER_PROTOCOL: SASL_PLAINTEXT
      KAFKA_SASL_MECHANISM_INTER_BROKER_PROTOCOL: SCRAM-SHA-512
      KAFKA_SASL_ENABLED_MECHANISMS: SCRAM-SHA-512
      KAFKA_OPTS: -Djava.security.auth.login.config=/etc/kafka/secrets/broker_jaas.conf
      KAFKA_AUTHORIZER_CLASS_NAME: kafka.security.auth.SimpleAclAuthorizer
      KAFKA_ALLOW_EVERYONE_IF_NO_ACL_FOUND: "false"
      KAFKA_SUPER_USERS: User:admin
    volumes:
      - ./etc/secrets:/etc/kafka/secrets
      - ./etc/sasl:/etc/kafka/sasl
    networks:
      - workshop-network-sasl

  kafka-2:
    image: confluentinc/cp-kafka:6.1.1
    hostname: kafka-2
    container_name:  kafka-2
    ports:
      - "29092:29092"
    depends_on:
      zookeeper:
        condition: service_healthy
    healthcheck:
      test: kafka-topics --bootstrap-server localhost:9092 --list --command-config /etc/kafka/sasl/admin-sasl.properties
      interval: 5s
      timeout: 5s
      retries: 10
    environment:
      KAFKA_BROKER_ID: 2
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: SASL_PLAINTEXT:SASL_PLAINTEXT,PLAINTEXT_HOST:SASL_PLAINTEXT,
      KAFKA_ADVERTISED_LISTENERS: SASL_PLAINTEXT://kafka-2:9092,PLAINTEXT_HOST://localhost:29092
      #     KAFKA_ADVERTISED_LISTENERS: SASL_PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
      KAFKA_DEFAULT_REPLICATION_FACTOR: 3
      KAFKA_NUM_PARTITIONS: 3
      KAFKA_MIN_INSYNC_REPLICAS: 2
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 3
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 2
      KAFKA_SECURITY_INTER_BROKER_PROTOCOL: SASL_PLAINTEXT
      KAFKA_SASL_MECHANISM_INTER_BROKER_PROTOCOL: SCRAM-SHA-512
      KAFKA_SASL_ENABLED_MECHANISMS: SCRAM-SHA-512
      KAFKA_OPTS: -Djava.security.auth.login.config=/etc/kafka/secrets/broker_jaas.conf
      KAFKA_AUTHORIZER_CLASS_NAME: kafka.security.auth.SimpleAclAuthorizer
      KAFKA_ALLOW_EVERYONE_IF_NO_ACL_FOUND: "false"
      KAFKA_SUPER_USERS: User:admin
    volumes:
      - ./etc/secrets:/etc/kafka/secrets
      - ./etc/sasl:/etc/kafka/sasl
    networks:
      - workshop-network-sasl

  kafka-3:
    image: confluentinc/cp-kafka:6.1.1
    hostname: kafka-3
    container_name:  kafka-3
    ports:
      - "39092:39092"
    depends_on:
      zookeeper:
        condition: service_healthy
    healthcheck:
      test: kafka-topics --bootstrap-server localhost:9092 --list --command-config /etc/kafka/sasl/admin-sasl.properties
      interval: 5s
      timeout: 5s
      retries: 10
    environment:
      KAFKA_BROKER_ID: 3
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: SASL_PLAINTEXT:SASL_PLAINTEXT,PLAINTEXT_HOST:SASL_PLAINTEXT,
      KAFKA_ADVERTISED_LISTENERS: SASL_PLAINTEXT://kafka-3:9092,PLAINTEXT_HOST://localhost:39092
      #     KAFKA_ADVERTISED_LISTENERS: SASL_PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
      KAFKA_DEFAULT_REPLICATION_FACTOR: 3
      KAFKA_NUM_PARTITIONS: 3
      KAFKA_MIN_INSYNC_REPLICAS: 2
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 3
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 2
      KAFKA_SECURITY_INTER_BROKER_PROTOCOL: SASL_PLAINTEXT
      KAFKA_SASL_MECHANISM_INTER_BROKER_PROTOCOL: SCRAM-SHA-512
      KAFKA_SASL_ENABLED_MECHANISMS: SCRAM-SHA-512
      KAFKA_OPTS: -Djava.security.auth.login.config=/etc/kafka/secrets/broker_jaas.conf
      KAFKA_AUTHORIZER_CLASS_NAME: kafka.security.auth.SimpleAclAuthorizer
      KAFKA_ALLOW_EVERYONE_IF_NO_ACL_FOUND: "false"
      KAFKA_SUPER_USERS: User:admin
    volumes:
      - ./etc/secrets:/etc/kafka/secrets
      - ./etc/sasl:/etc/kafka/sasl
    networks:
      - workshop-network-sasl

  schema-registry:
    image: confluentinc/cp-schema-registry:6.1.1
    hostname: schema-registry
    container_name: schema-registry
    healthcheck:
      test: curl http://schema-registry:8081/subjects || exit 1
      interval: 5s
      timeout: 5s
      retries: 30
    depends_on:
      zookeeper:
        condition: service_healthy
      kafka-1:
        condition: service_healthy
    ports:
      - "8081:8081"
    environment:
      SCHEMA_REGISTRY_HOST_NAME: schema-registry
      SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL: zookeeper:2181
      SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS: SASL_PLAINTEXT://kafka-1:9092
      SCHEMA_REGISTRY_KAFKASTORE_SECURITY_PROTOCOL: SASL_PLAINTEXT
      SCHEMA_REGISTRY_KAFKASTORE_SASL_MECHANISM: SCRAM-SHA-512
      SCHEMA_REGISTRY_KAFKASTORE_SASL_JAAS_CONFIG: "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"admin\" password=\"admin-secret\";"
    volumes:
      - ./etc/secrets:/etc/kafka/secrets
    networks:
      - workshop-network-sasl

  base:
    image: confluentinc/cp-kafka-connect:6.1.1
    container_name: base
    hostname: base
    command: /bin/sh
    tty: true
    volumes:
      - ./etc/sasl:/etc/kafka/sasl
    networks:
      - workshop-network-sasl

networks:
  workshop-network-sasl:
    driver: bridge
    name: workshop-network-sasl