#### This exercise aims at producing and consuming string records using the Kafka console producer and Kafka console consumer

#### Start the Kafka cluster
In the CONSOLE directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  
The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry 
The docker-compose file contains also a base container that allows to run Kafka commands

#### Interact with Kafka cluster
Get into the base container    
```docker exec -it base bash```
		
#### Create a topic
Create a topic named consoleTopic using the kafka-topics command, replicated 3 times and with 3 partitions  
```kafka-topics --create --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092 --topic consoleTopic --replication-factor 3 --partitions 3```

#### List existing topics
List the existing topics using the --list option of the kafka-topics command  
```kafka-topics --list --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092```  

#### Produce records
Using the Kafka console producer, send records to the topic consoleTopic  
 ```kafka-console-producer --broker-list kafka-1:9092,kafka-2:9092,kafka-3:9092 --topic consoleTopic```  

Do not close the console  

#### Consume records
Open another console and get into the base container    
```docker exec -it base bash```  

Using the Kafka console consumer, get records from the topic consoleTopic with a consumer group group1  
```kafka-console-consumer --bootstrap-server  kafka-1:9092,kafka-2:9092,kafka-3:9092 --topic consoleTopic --group group1```  
Note that nothing happens, no record is consumed  
Using the active console producer, send another record  
Note that this time, the record is consumed  

Stop the active consumer, and consume records from the topic consoleTopic with a consumer group group2, add the --from-beginning option  
```kafka-console-consumer --bootstrap-server  kafka-1:9092,kafka-2:9092,kafka-3:9092 --topic consoleTopic --group group2 --from-beginning```  
This time, all the records are consumed!  

Open another console, and get into the base container    
```docker exec -it base bash```  

Consume records from the topic consoleTopic with the same consumer group group2  
```kafka-console-consumer --bootstrap-server  kafka-1:9092,kafka-2:9092,kafka-3:9092 --topic consoleTopic --group group2 --from-beginning```  

Send some records, note the records are consumed by only one consumer of the group group2  
Stop the console producer (CTRL+C) and display how partitions are distributed between consumers of the group group2   
 ```kafka-consumer-groups --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092 --describe --group group2```  

#### Stop and remove containers 
 ```docker-compose stop```   
 ```docker-compose rm -f```
