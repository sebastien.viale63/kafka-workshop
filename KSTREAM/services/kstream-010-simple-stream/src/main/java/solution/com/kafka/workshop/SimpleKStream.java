package solution.com.kafka.workshop;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class SimpleKStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(SimpleKStream.class);

    private String topicIn = TopicsNames.SIMPLE_STREAM_IN;
    private String topicOut = TopicsNames.SIMPLE_STREAM_OUT;
    
    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topicIn);
        Topics.createTopics(topicOut);

        try {
            // TODO Create a Topology object, use the getTopology() method
            Topology topology = getTopology();
            logger.info("topology: {}", topology.describe());
            // TODO Create KafkaStreams instance with properties, TIPS: use createStream method
            KafkaStreams streams = new KafkaStreams(topology, configureStream());
            // TODO Start the stream
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopology() {
        final StreamsBuilder builder = new StreamsBuilder();
        // TODO Create KStream instance with builder and consume records from topicIn
        KStream<String, String> stream = builder.stream(topicIn, Consumed.with(Serdes.String(), Serdes.String()));

        // TODO Produce records to topicOut with no transformation
        stream.to(topicOut, Produced.with(Serdes.String(), Serdes.String()));
        return builder.build();

    }

    public Properties configureStream() {

        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // TODO Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "applicationId");
        // TODO Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // TODO Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return streamsConfiguration;
    }
}
