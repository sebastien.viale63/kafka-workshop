package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatEnriched;
import org.apache.kafka.streams.kstream.ValueJoiner;

/*
    TODO Implements the ValueJoiner<V1, V2, VR> interface
 */
public class CustomJoiner implements ValueJoiner<WorkshopStat, WorkshopRef, WorkshopStatEnriched> {

    /*
        TODO Override the apply method to return an instance of WorkshopStatEnriched object
     */
    @Override
    public WorkshopStatEnriched apply(WorkshopStat workshopStat, WorkshopRef workshopRef) {
        return WorkshopStatEnriched.newBuilder()
                .setId(workshopStat.getId())
                .setDescription(workshopStat.getDescription())
                .setStat(workshopStat.getStat())
                .setLongDescription(workshopRef.getDescription())
                .build();
    }
}
