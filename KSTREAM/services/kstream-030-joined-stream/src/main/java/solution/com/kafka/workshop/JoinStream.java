package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatAdd;
import com.kafka.workshop.avro.WorkshopStatEnriched;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.StreamExecutionContext;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class JoinStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(JoinStream.class);

    private final String topic_stream_left_in = TopicsNames.JOIN_STREAM_LEFT_IN;
    private final String topic_stream_right_in = TopicsNames.JOIN_STREAM_RIGHT_IN;
    private final String topic_table_in = TopicsNames.JOIN_TABLE_IN;
    private final String topic_stream_table_out = TopicsNames.JOIN_STREAM_TABLE_OUT;
    private final String topic_stream_stream_out = TopicsNames.JOIN_STREAM_STREAM_OUT;
    
    public final int windowDuration = 10;
    public final int partitionNumber = 3;


    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic_stream_left_in);
        Topics.createTopics(topic_stream_right_in, 2);
        Topics.createCompactTopics(topic_table_in);
        Topics.createTopics(topic_stream_table_out);
        Topics.createTopics(topic_stream_stream_out);
        
        try {
            // init serdes
            Map<String, String> serdesConfig = new HashMap<>();
            serdesConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG,
                    KafkaProperties.SCHEMA_REGISTRY);
            StreamExecutionContext.setSerdesConfig(serdesConfig);
            StreamExecutionContext.setPartitionNumber(partitionNumber);
            
        //    Topology topology = getTopologyStreamTableJoin();
            Topology topology = getTopologyStreamStreamJoin();
            logger.info("topology: {}", topology.describe());
            KafkaStreams streams = new KafkaStreams(topology, configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     *
     * @return
     */
    public Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "merge-app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // TODO Set the replication factor for internal topics to 3
        streamsConfiguration.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 3);
        // TODO Set the min in sync replicas for internal topics to 2
        streamsConfiguration.put(StreamsConfig.topicPrefix(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG), 2);
        // TODO Set default tmp folder
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);
        // TODO Set the timestamp extractor
        streamsConfiguration.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, JoinTimestampExtractor.class.getName());
        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopologyStreamTableJoin() {
        final StreamsBuilder builder = new StreamsBuilder();
        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_stream_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, WorkshopStat> statStream = builder
                .stream(topic_stream_left_in, Consumed.with(Serdes.String(), SerdesUtils.<WorkshopStat>getSerdes(false)));
        /*
            TODO Create a KTable instance from the StreamBuilder instance
                 Consume topic_table_in by specifying the Serdes for key and value with Consumed.with() method
                 Materialized the KTabke

        */
        KTable<String, WorkshopRef> refTable = builder.table(topic_table_in,
                Consumed.with(Serdes.String(), SerdesUtils.<WorkshopRef>getSerdes(false)),
                Materialized.<String, WorkshopRef, KeyValueStore<Bytes, byte[]>>as("MERGE_REF_TOPIC_TABLE")
                .withKeySerde(Serdes.String()).withValueSerde(SerdesUtils.<WorkshopRef>getSerdes(false)));

        /*
            TODO Configure the CustomJoiner class and create an instance
        */
        CustomJoiner customJoiner = new CustomJoiner();
        /*
            TODO Join of statStream and refTable
                 Use the join() method on the statStream instance,
                 pass the three following arguments: refTable, customJoiner and Joined.with()
        */
        KStream<String, WorkshopStatEnriched> enrichedStream = statStream.join(refTable, customJoiner,
                Joined.with(Serdes.String(), 
                        SerdesUtils.<WorkshopStat>getSerdes(false),
                        SerdesUtils.<WorkshopRef>getSerdes(false))
                        .withName("STREAM_TABLE_ENRICH_JOIN"));
        
        /*
            TODO Produce the stream to merge-out-topic
        */
        enrichedStream.to(topic_stream_table_out, Produced.with(Serdes.String(), SerdesUtils.<WorkshopStatEnriched>getSerdes(false)));
        return builder.build();
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopologyStreamStreamJoin() {
        final StreamsBuilder builder = new StreamsBuilder();
        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_stream_left_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, WorkshopStat> statStreamLeft = builder
                .stream(topic_stream_left_in, Consumed.with(Serdes.String(), SerdesUtils.<WorkshopStat>getSerdes(false)));
        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_stream_right_in by specifying the Serdes for key and value with Consumed.with() method
                 Repartition the topic using the .repartition() method
        */
        KStream<String, WorkshopStatAdd> statStreamRight = builder
                .stream(topic_stream_right_in, Consumed.with(Serdes.String(), SerdesUtils.<WorkshopStatAdd>getSerdes(false)))
                .repartition(Repartitioned.<String, WorkshopStatAdd>numberOfPartitions(StreamExecutionContext.getPartitionNumber())
                        .withName("CUSTOM-REPARTITION").withKeySerde(Serdes.String()).withValueSerde(SerdesUtils.<WorkshopStatAdd>getSerdes(false)));
        /*
            TODO Join of statStream and refTable
                 Use the join() method on the statStream instance,
                 pass the three following arguments: refTable, customJoiner and Joined.valueSerde().keySerde()
        */
        KStream<String, WorkshopStatEnriched> enrichedStream = statStreamLeft
                .join(statStreamRight,
                        (left, right) -> WorkshopStatEnriched.newBuilder()
                                .setId(left.getId())
                                .setDescription(left.getDescription())
                                .setStat(left.getStat())
                                .setStatAdd(right.getStatAdd())
                                .build(),
                        JoinWindows.of(Duration.ofSeconds(windowDuration)),
                        StreamJoined.with(Serdes.String(),
                                SerdesUtils.<WorkshopStat>getSerdes(false),
                                SerdesUtils.<WorkshopStatAdd>getSerdes(false))
                                .withName("STREAM_STREAM_ENRICH_JOIN"));
        
        /*
            TODO Produce the stream to merge-out-topic
        */
        enrichedStream.to(topic_stream_stream_out, Produced.with(Serdes.String(), SerdesUtils.<WorkshopStatEnriched>getSerdes(false)));
        return builder.build();

    }
}


