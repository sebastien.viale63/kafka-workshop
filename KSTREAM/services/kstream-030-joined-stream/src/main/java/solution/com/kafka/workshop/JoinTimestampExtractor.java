package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatAdd;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class JoinTimestampExtractor implements TimestampExtractor {
    @Override
    public long extract(ConsumerRecord<Object, Object> record, long previousTimestamp) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String eventTime;
            if (record.value() instanceof WorkshopStat) {
                eventTime = ((WorkshopStat) record.value()).getTimestamp();
                return sdf.parse(eventTime).getTime();
            } else if (record.value() instanceof WorkshopStatAdd) {
                eventTime = ((WorkshopStatAdd) record.value()).getTimestamp();
                return sdf.parse(eventTime).getTime();
            } else {
                return record.timestamp();
            }
        } catch (ParseException e) {
            return 0;
        }
    }
}
