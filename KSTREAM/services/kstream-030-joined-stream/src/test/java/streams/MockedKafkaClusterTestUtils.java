package streams;

import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatAdd;
import com.kafka.workshop.avro.WorkshopStatEnriched;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.StreamExecutionContext;
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import solution.com.kafka.workshop.JoinStream;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class MockedKafkaClusterTestUtils {


    protected TopologyTestDriver testDriver;

    protected final String topic_stream_left_in = TopicsNames.JOIN_STREAM_LEFT_IN;
    protected final String topic_stream_right_in = TopicsNames.JOIN_STREAM_RIGHT_IN;
    protected final String topic_table_in = TopicsNames.JOIN_TABLE_IN;
    protected final String topic_stream_table_out = TopicsNames.JOIN_STREAM_TABLE_OUT;
    protected final String topic_stream_stream_out = TopicsNames.JOIN_STREAM_STREAM_OUT;
  
    TestInputTopic<String, WorkshopStat> stream_left_in;

    TestInputTopic<String, WorkshopStatAdd> stream_right_in;


    TestInputTopic<String, WorkshopRef> table_in;
    
    TestOutputTopic<String, WorkshopStatEnriched> stream_table_out;

    TestOutputTopic<String, WorkshopStatEnriched> stream_stream_out;

    
    protected Map<String, String> serdeConfig;

    protected JoinStream kafkaStreamBuilder;
    
    protected Properties properties;
    
    @BeforeEach
    public void testSetup() {
        kafkaStreamBuilder = new JoinStream();
        // Create an instance of TopologyTestDriver with the topology to test
        properties = kafkaStreamBuilder.configureStream();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "test" + TopicsNames.PREFIX + "window-app-id");
        properties.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);

        this.serdeConfig = Collections.singletonMap("schema.registry.url", "mock://DUMMY_URL");
        StreamExecutionContext.setSerdesConfig(this.serdeConfig);
        StreamExecutionContext.setPartitionNumber(1);

    }

    @AfterEach
    public void close() {
        testDriver.close();

        FileUtils.deleteQuietly(new File(KafkaProperties.STORE_DIR));

        MockSchemaRegistry.dropScope(this.getClass().getName());
    }
    
    
    protected static String getTimestamp(int minutes){
        LocalDateTime localDateTime = LocalDateTime.now().minusHours(3).withMinute(0);
        final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return sdf.format(localDateTime.plusMinutes(minutes));
    }

}
