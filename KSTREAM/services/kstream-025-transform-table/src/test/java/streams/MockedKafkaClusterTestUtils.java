package streams;

import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.StreamExecutionContext;
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import solution.com.kafka.workshop.TransformTable;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

public class MockedKafkaClusterTestUtils {

    protected TopologyTestDriver testDriver;

    private final String topic_in = TopicsNames.TRANSFORM_STREAM_IN;
    private final String topic_out = TopicsNames.TRANSFORM_STREAM_OUT;

    TestInputTopic<String, String> transformIn;
   
    TestOutputTopic<Long, WorkshopValue> transformOut;

    protected Map<String, String> serdeConfig;

    protected TransformTable kafkaStreamBuilder;

    protected Properties properties;
    
    @BeforeEach
    public void testSetup() {
        kafkaStreamBuilder = new TransformTable();
        properties = kafkaStreamBuilder.configureStream();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "test" + TopicsNames.PREFIX + "transform-app-id");
        properties.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);

        this.serdeConfig = Collections.singletonMap("schema.registry.url", "mock://DUMMY_URL");
        StreamExecutionContext.setSerdesConfig(this.serdeConfig);
        
        testDriver = new TopologyTestDriver(kafkaStreamBuilder.getTopology(), properties);

        transformIn = testDriver.createInputTopic(topic_in,
                Serdes.String().serializer(), Serdes.String().serializer());
        transformOut = testDriver.createOutputTopic(topic_out, Serdes.Long().deserializer(), SerdesUtils.<WorkshopValue>getSerdes(false).deserializer());
     
    }

    @AfterEach
    public void close() {
        testDriver.close();

        FileUtils.deleteQuietly(new File(KafkaProperties.STORE_DIR));

        MockSchemaRegistry.dropScope(this.getClass().getName());
    }



}
