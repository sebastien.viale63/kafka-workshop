package streams;

import com.kafka.workshop.avro.WorkshopValue;
import org.apache.kafka.streams.KeyValue;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestingTableTest extends MockedKafkaClusterTestUtils {

    @Test
    public void consolidatedDatasTopicTest() {

        // long desc start with a digit => ok
        String key = "key1";
        String value = "111" + "::1 test long description ok::test short description::test ref";
        transformIn.pipeInput(key, value);

        // long desc does not start with a digit => ko
        key = "key2";
        value = "222" + "::test long description ko::test short description::test ref";
        transformIn.pipeInput(key, value);

        List<KeyValue<Long, WorkshopValue>> results = transformOut.readKeyValuesToList();
        
        WorkshopValue expected = WorkshopValue.newBuilder()
                .setId(111)
                .setLongDescription("1 test long description ok")
                .setShortDescription("test short description")
                .setReference("newRef_test ref").build();

        assertEquals(1, results.size());
        assertEquals(expected, results.get(0).value);

    }



}


