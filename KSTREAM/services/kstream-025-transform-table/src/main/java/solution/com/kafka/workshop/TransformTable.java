package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.StreamExecutionContext;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class TransformTable implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(TransformTable.class);

    private final String topic_in = TopicsNames.TRANSFORM_TABLE_IN;
    private final String topic_out = TopicsNames.TRANSFORM_TABLE_OUT;

    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic_in);
        Topics.createTopics(topic_out);

        try {
            // init serdes
            Map<String, String> serdesConfig = new HashMap<>();
            serdesConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG,
                    KafkaProperties.SCHEMA_REGISTRY);
            StreamExecutionContext.setSerdesConfig(serdesConfig);

            // Create KafkaStreams instance
            Topology topology = getTopology();
            logger.info("topology: {}", topology.describe());
            KafkaStreams streams = new KafkaStreams(topology, configureStream());

            // Start the stream
            streams.start();

            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     *
     * @return
     */
    public Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        // Set the bootstrap servers using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "transform-app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // TODO Set default tmp folder
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);
        // TODO Play with cache.max.bytes.buffering property
        streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 10485760);

        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopology() {
        // TODO Create StreamsBuilder instance
        final StreamsBuilder builder = new StreamsBuilder();

        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
                 Materialized the KTabke
        */
        KTable<String, String> table = builder.table(topic_in,
                Consumed.with(Serdes.String(), Serdes.String()),
                Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as("TABLE-IN").withValueSerde(Serdes.String()).withKeySerde(Serdes.String()));
       
        /*
            TODO Change the Reference property of workshopValue object, use the setRef method
         */
        table.mapValues(value -> convertWorkshop(value))
                /*
                    TODO Filter the key, accept only ones that does not start with "5"
                 */
                .filter((key, value) -> !key.startsWith("5"))
                /*
                    TODO Convert the KTable to a KStream object
                         Print the k and value using the peek() method 
                 */
                .toStream()
                .peek((k, v) -> System.out.println("transfor to stream " + k + " == " + v))
                /*
                    TODO Produce the new stream to topic_out by specifying the Serdes for key and value with Produces.with() method
                */
                .to(topic_out, Produced.with(Serdes.String(), SerdesUtils.<WorkshopValue>getSerdes(false)));

        return builder.build();

    }


    /**
     * Convert a String value object to a WorkshopValue object
     *
     * @param value
     * @return
     */
    public static WorkshopValue convertWorkshop(String value) {
        String[] valueArray = value.split("::");
        Long id = Long.parseLong(valueArray[0]);
        String longDesc = valueArray[1];
        String shortDesc = valueArray[2];
        String reference = valueArray[3];
        // TODO Create a WorkshopValue object
        WorkshopValue workshopValue = WorkshopValue
                .newBuilder()
                .setId(id)
                .setShortDescription(shortDesc)
                .setLongDescription(longDesc)
                .setReference(reference)
                .build();
        return workshopValue;
    }

}

