package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.StreamExecutionContext;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class TransformerStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(TransformerStream.class);

    private final String topic_in = TopicsNames.DEDUPLICATE_TOPIC_IN;
    private final String topic_out = TopicsNames.DEDUPLICATE_TOPIC_OUT;

    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic_in);
        Topics.createTopics(topic_out);

        try {

            // init serdes
            Map<String, String> serdesConfig = new HashMap<>();
            serdesConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG,
                    KafkaProperties.SCHEMA_REGISTRY);
            StreamExecutionContext.setSerdesConfig(serdesConfig);
            
            Topology topology = getTopology();
            logger.info("topology: {}", topology.describe());
            KafkaStreams streams = new KafkaStreams(topology, configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     * @return
     */
    public Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "deduplicate-app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // Set the replication factor for internal topics to 3
        streamsConfiguration.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 3);
        // Set the min in sync replicas for internal topics to 2
        streamsConfiguration.put(StreamsConfig.topicPrefix(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG), 2);
        // Set default tmp folder
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);
        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopology() {

        final StreamsBuilder builder = new StreamsBuilder();

        /*
            TODO Create storeBuilder to manage record deduplication
                 Use the  Stores.keyValueStoreBuilder() static method
                 Set an persistentKeyValueStore
         */
        final StoreBuilder<KeyValueStore<String, WorkshopValue>> deduplicateStoreBuilder =
                Stores.keyValueStoreBuilder(
                        Stores.persistentKeyValueStore("Deduplicate"),
                        Serdes.String(),
                        SerdesUtils.<WorkshopValue>getSerdes(false));

        // TODO Add the state store to builder
        builder.addStateStore(deduplicateStoreBuilder);

        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, WorkshopValue> stream = builder
                .stream(topic_in, Consumed.with(Serdes.String(), SerdesUtils.<WorkshopValue>getSerdes(false)));
      
         /*
            TODO 
                 Call the API Processor using the transform(() -> new DeduplicateValueTransformer(), storeName) method
                 Produce the new stream to topic_out by specifying the Serdes for key and value with Produces.with() method
         */
        stream.transform(() -> new DeduplicateValueTransformer(), "Deduplicate")
                .to(topic_out, Produced.with(Serdes.String(), SerdesUtils.<WorkshopValue>getSerdes(false)));

        return builder.build();
    }
    
}

