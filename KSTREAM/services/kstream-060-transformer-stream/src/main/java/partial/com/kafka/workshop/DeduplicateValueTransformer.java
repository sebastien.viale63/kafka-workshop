package partial.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopValue;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

/*
    TODO Implements the Transformer<K, V, R> interface
 */
public class DeduplicateValueTransformer implements Transformer<String, WorkshopValue, KeyValue<String, WorkshopValue>> {

    private static final Logger logger = LoggerFactory.getLogger(DeduplicateValueTransformer.class);

    private ProcessorContext context;

    private KeyValueStore<String, WorkshopValue> kvStore;

    /*
        TODO Override the init method
             Get the context and set it to the context local variable
             Get the state store from the context and set it to the kvStore local varaible
             Call the schedule() method of the context:
               punctuate each 5 seconds
               set the PuntuationType to WALL_CLOCK_TIME
               in the call back method, iterate through all the store
                                                                        forward the value to continue the stream
                                                                        delete the forwarded value from the store
     */
    @Override
    public void init(ProcessorContext context) {

        this.context = context;

        this.kvStore = (KeyValueStore<String , WorkshopValue>) context.getStateStore("Deduplicate");

        // schedule a punctuate() method every 5 seconds based wall clock time
        this.context.schedule(Duration.ofMillis(Long.valueOf(5000)),
                PunctuationType.WALL_CLOCK_TIME, (timestamp) -> {
                    KeyValueIterator<String, WorkshopValue> iter = this.kvStore.all();
                    logger.info("executing punctuation");
                    while (iter.hasNext()) {
                        KeyValue<String, WorkshopValue> entry = iter.next();
                        logger.info("forwarding {} {}", entry.key, entry.value);
                        this.context.forward(entry.key, entry.value);
                        this.kvStore.delete(entry.key);
                    }
                    iter.close();
                });
    }

    /*
       TODO Override the transform method
            Put a new key / value to the store if the key does not exist
            Return null
    */
    @Override
    public KeyValue<String, WorkshopValue> transform(String key, WorkshopValue value) {
        
        return null;
    }


    /*
        TODO Override the close method, do nothing
     */
    @Override
    public void close() {}
}
