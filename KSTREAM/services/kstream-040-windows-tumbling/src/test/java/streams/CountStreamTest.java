package streams;

import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.utils.SerdesUtils;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CountStreamTest extends MockedKafkaClusterTestUtils {

    @Test
    public void countTest() {
        System.out.println("AAAAAAAAAAA == " + kafkaStreamBuilder.getTopologyCount().describe());
        String key;

        for(String s: getList()) {

            key = getAlphaNumericString(3);
            String[] valueArray = s.split("::");
            WorkshopStat value = WorkshopStat.newBuilder()
                    .setIdent(valueArray[0])
                    .setReference(valueArray[1])
                    .setStat(Double.parseDouble(valueArray[2]))
                    .setTimestamp(valueArray[3])
                    .build();
            windowIn.pipeInput(key, value);
        }
        
        List<KeyValue<String, String>> results = countOut.readKeyValuesToList();

        assertEquals(results.size(), 8);
        assertEquals(results.get(2).value, "3");
        assertEquals(results.get(5).value, "3");
        assertEquals(results.get(7).value, "2");
    }

    @BeforeEach
    public void testSetup() {

        super.testSetup();
        testDriver = new TopologyTestDriver(kafkaStreamBuilder.getTopologyCount(), properties);

        windowIn = testDriver.createInputTopic(topic_in,
                Serdes.String().serializer(), SerdesUtils.<WorkshopStat>getSerdes(false).serializer());
        countOut = testDriver.createOutputTopic(topic_count, Serdes.String().deserializer(), Serdes.String().deserializer());
        
    }

}


