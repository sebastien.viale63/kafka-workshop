#### This exercise aims at writing a KSTREAM java application counting records received by key
The objective of this exercise is to:
- read records from window_topic topic with KafkaStream application  
  The topic contains String key / WorkshopValue value pairs  
  count the number of records by key in a tumbling window (10 minutes)  
    
#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  

#### Configure the KSTREAM topology
Configure the stream topology following the TODO instructions in the partial.com.kafka.workshop.TumblingWindows java class

#### Configure the KSTREAM topology and the Join between the KStream and the KTable
Follow the TODO instructions in the partial.com.kafka.workshop.TumblingWindows java class  

#### Start the Stream application
Start the application  

#### Consume example records
Use the com.kafka.workshop.init.consumer.WindowTumblingStreamConsumerApplication class to consumer records from window_count topic     
Check that merging and filtering are made correctly    

#### Produce example records  
Use the com.kafka.workshop.com.kafka.workshop.init.producer.WindowTumblingProducerApplication class to produce records to window_topic topic  
It produces a list of WorkshopStatValue representing for a statistic by key in on a period of one hour  

#### Play with CACHE_MAX_BYTES_BUFFERING_CONFIG parameter
Stop all applications  
Delete the topics  
```kafka-topics --delete --topic window_count --bootstrap-server kafka1:19092```  
```kafka-topics --delete --topic window_topic --bootstrap-server kafka1:19092```  
Uncomment the CACHE_MAX_BYTES_BUFFERING_CONFIG config and set it to 0   
Restart all stages  
The Streams output cache is disable, this optimize the Streams app for latency at the expense of throughput  
Each row will be displayed  