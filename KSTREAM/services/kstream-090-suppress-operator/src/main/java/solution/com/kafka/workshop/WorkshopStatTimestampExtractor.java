package solution.com.kafka.workshop;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;

import java.time.Instant;

public class WorkshopStatTimestampExtractor implements TimestampExtractor {

    @Override
    public long extract(ConsumerRecord<Object, Object> record, long partitionTime) {

        Workshop workshop = (Workshop) record.value();
        if (workshop != null && workshop.getTimestamp() != null) {
            String timestamp = workshop.getTimestamp();
            System.out.println("Extracting timestamp: " + timestamp);
            return Instant.parse(timestamp).toEpochMilli();
        }
        // fallback to stream time
        return partitionTime;
    }
}

