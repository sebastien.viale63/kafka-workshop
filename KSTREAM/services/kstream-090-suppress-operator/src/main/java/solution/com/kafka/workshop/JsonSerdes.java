package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopStat;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public class JsonSerdes {

    public static Serde<Workshop> Workshop() {
        JsonSerializer<Workshop> serializer = new JsonSerializer<>();
        JsonDeserializer<Workshop> deserializer = new JsonDeserializer<>(Workshop.class);
        return Serdes.serdeFrom(serializer, deserializer);
    }

}