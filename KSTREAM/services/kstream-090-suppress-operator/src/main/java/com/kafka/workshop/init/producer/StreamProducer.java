package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class StreamProducer {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private final String topic_in = TopicsNames.WINDOWS_STREAM_IN;;

    public static void main(String[] args){
        StreamProducer streamProducer = new StreamProducer();
        streamProducer.run();
    }
    
    public void run() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopStat> producer = new KafkaProducer<>(props);

        try {

            String key;

            for(String s: getList()) {

                key = getAlphaNumericString(3);
                String[] valueArray = s.split("::");
                WorkshopStat value = WorkshopStat.newBuilder()
                        .setIdent(valueArray[0])
                        .setReference(valueArray[1])
                        .setStat(Double.parseDouble(valueArray[2]))
                        .setTimestamp(valueArray[3])
                        .build();
                // Send record
                ProducerRecord<String, WorkshopStat> record = new ProducerRecord<>(topic_in, key, value);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }
    }

    private List<String> getList(){
        List<String> list = new ArrayList<>();
        LocalDateTime localDateTime = LocalDateTime.now().minusHours(3).withMinute(0);
        final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        
        list.add("id01::ref01::8.2::" + sdf.format(localDateTime));
        list.add("id01::ref01::4.5::" + sdf.format(localDateTime.plusMinutes(5)));
        list.add("id01::ref01::5.1::" + sdf.format(localDateTime.plusMinutes(8)));
        list.add("id01::ref01::2.0::" + sdf.format(localDateTime.plusMinutes(11)));
        list.add("id01::ref01::8.3::" + sdf.format(localDateTime.plusMinutes(15)));
        list.add("id01::ref01::3.4::" + sdf.format(localDateTime.plusMinutes(18)));
        list.add("id01::ref01::4.2::" + sdf.format(localDateTime.plusMinutes(21)));        
        list.add("id01::ref01::7.6::" + sdf.format(localDateTime.plusMinutes(25)));

        list.add("id02::ref02::4.9::" + sdf.format(localDateTime.plusMinutes(1)));
        list.add("id02::ref02::5.6::" + sdf.format(localDateTime.plusMinutes(3)));
        list.add("id02::ref02::9.0::" + sdf.format(localDateTime.plusMinutes(5)));
        list.add("id02::ref02::6.5::" + sdf.format(localDateTime.plusMinutes(8)));
        list.add("id02::ref02::2.1::" + sdf.format(localDateTime.plusMinutes(11)));
        
        list.add("id03::ref03::3.6::2019-04-25T22:20:00-0000");
        list.add("id03::ref03::6.0::2019-04-25T22:21:00-0000");
        list.add("id03::ref03::7.0::2019-04-25T22:22:00-0000");
        list.add("id03::ref03::4.6::2019-04-25T22:23:00-0000");
        list.add("id03::ref03::7.1::2019-04-25T22:24:00-0000");
        list.add("id04::ref04::9.9::2019-04-25T21:15:00-0000");
        list.add("id04::ref04::8.6::2019-04-25T21:16:00-0000");
        list.add("id04::ref04::4.2::2019-04-25T21:17:00-0000");
        list.add("id04::ref04::7.0::2019-04-25T21:18:00-0000");
        list.add("id04::ref04::9.5::2019-04-25T21:19:00-0000");
        list.add("id04::ref04::3.2::2019-04-25T21:20:00-0000");
        list.add("id05::ref05::3.5::2019-04-25T13:00:00-0000");
        list.add("id05::ref05::4.0::2019-04-25T13:07:00-0000");
        list.add("id05::ref05::5.1::2019-04-25T13:30:00-0000");
        list.add("id05::ref05::2.0::2019-04-25T13:34:00-0000");
        return list;
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }


}
