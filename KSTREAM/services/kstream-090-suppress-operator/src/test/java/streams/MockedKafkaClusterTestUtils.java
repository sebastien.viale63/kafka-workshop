package streams;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.StreamExecutionContext;
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import solution.com.kafka.workshop.SuppressOperator;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class MockedKafkaClusterTestUtils {


    protected TopologyTestDriver testDriver;

    public final String topic_in = TopicsNames.WINDOWS_STREAM_IN;
    public final String topic_count = TopicsNames.WINDOWS_STREAM_COUNT_OUT;
  
    TestInputTopic<String, String> windowIn;
   
    TestOutputTopic<String, String> countOut;

    protected Map<String, String> serdeConfig;

    protected SuppressOperator kafkaStreamBuilder;
    
    protected Properties properties;
    
    @BeforeEach
    public void testSetup() {
        kafkaStreamBuilder = new SuppressOperator();
        // Create an instance of TopologyTestDriver with the topology to test
        properties = kafkaStreamBuilder.configureStream();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "test" + TopicsNames.PREFIX + "window-app-id");
        properties.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);

        this.serdeConfig = Collections.singletonMap("schema.registry.url", "mock://DUMMY_URL");
        StreamExecutionContext.setSerdesConfig(this.serdeConfig);

    }

    @AfterEach
    public void close() {
        testDriver.close();

        FileUtils.deleteQuietly(new File(KafkaProperties.STORE_DIR));

        MockSchemaRegistry.dropScope(this.getClass().getName());
    }

    protected static List<String> getList() {
        List<String> list = new ArrayList<>();
        LocalDateTime localDateTime = LocalDateTime.now().withHour(12).withMinute(00).withSecond(0);
        final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        list.add("id01::ref01::8.2::" + sdf.format(localDateTime));
        list.add("id01::ref01::4.5::" + sdf.format(localDateTime.plusMinutes(5)));
        list.add("id01::ref01::5.3::" + sdf.format(localDateTime.plusMinutes(8)));
        list.add("id01::ref01::2.1::" + sdf.format(localDateTime.plusMinutes(11)));
        list.add("id01::ref01::8.3::" + sdf.format(localDateTime.plusMinutes(15)));
        list.add("id01::ref01::3.6::" + sdf.format(localDateTime.plusMinutes(18)));
        list.add("id01::ref01::4.4::" + sdf.format(localDateTime.plusMinutes(21)));
        list.add("id01::ref01::7.6::" + sdf.format(localDateTime.plusMinutes(25)));

        return list;

    }


    protected static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

}
