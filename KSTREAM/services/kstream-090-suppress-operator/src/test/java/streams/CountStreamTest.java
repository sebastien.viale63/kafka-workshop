package streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class CountStreamTest extends MockedKafkaClusterTestUtils {

    @Test
    public void countTest() {

        LocalDateTime localDateTime = LocalDateTime.now().withHour(8).withMinute(0).withSecond(0);

        ZonedDateTime zdt = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());

        String json_v1 = "{\n" +
                "\"timestamp\": \"2020-11-05T09:00:00.000Z\"\n" +
                "}";

        String json_v2 = "{\n" +
                "\"timestamp\": \"2020-11-05T09:00:58.000Z\"\n" +
                "}";

        String json_v3 = "{\n" +
                "\"timestamp\": \"2020-11-05T09:01:55.000Z\"\n" +
                "}";

        String json_v4 = "{\n" +
                "\"timestamp\": \"2020-11-05T09:02:04.000Z\"\n" +
                "}";

        String json_v5 = "{\n" +
                "\"timestamp\": \"2020-11-05T09:05:00.000Z\"\n" +
                "}";

//        windowIn.pipeInput("A", json_v1, zdt.toInstant().toEpochMilli());
//
//        windowIn.pipeInput("A", json_v2, zdt.toInstant().plusSeconds(500).toEpochMilli());
        
        windowIn.pipeInput("A", json_v1);
        
        windowIn.pipeInput("A", json_v2);

        windowIn.pipeInput("B", json_v4);
        
        windowIn.pipeInput("A", json_v3);

        windowIn.pipeInput("B", json_v5);
        

    }


//    @Test
//    public void countTest() {
//
//        LocalDateTime localDateTime = LocalDateTime.now().withHour(8).withMinute(0).withSecond(0);
//    
//        ZonedDateTime zdt = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
//
//        
//        windowIn.pipeInput("A", "v1", zdt.toInstant().toEpochMilli());
//        
//        windowIn.pipeInput(
//                /* key */       "A",
//                /* value */     "v2",
//                /* timestamp */ zdt.toInstant().plusSeconds(30).toEpochMilli()
//        );
//        // Stream time is now 10L
//     
//        
//        windowIn.pipeInput("A", "v3", zdt.toInstant().plusSeconds(90).toEpochMilli());
//        // Stream time is now 11L
//
//        windowIn.pipeInput("B", "v5", zdt.toInstant().plusSeconds(124).toEpochMilli());
//        
//        windowIn.pipeInput("A", "v4", zdt.toInstant().plusSeconds(121).toEpochMilli());
//        // Stream time is now 12L
//
//        // To keep up with the example, we advance stream time to 13
//        // by sending a record with a dummy key
//        windowIn.pipeInput("A", "anything", zdt.toInstant().plusSeconds(500).toEpochMilli());
//        // Stream time is now 13L
//
//    //     windowIn.pipeInput("A", "v5", zdt.toInstant().plusSeconds(239).toEpochMilli());
//        // Stream time is still 13L
//
//     //   windowIn.pipeInput("B", "v4", zdt.toInstant().plusSeconds(241).toEpochMilli());
//
//        // If you check driver.readOutput any time up to and including now, it
//        // returns null, since suppress won't emit anything until the window closes, 
//        // which happens at time 14
//
//  //      windowIn.pipeInput("foo", "anything", 14L);
//        // Stream time is now 14L
//        // Now, driver.readOutput returns the count of events for "A" in the 
//        // window starting at time 10, which is 3.
//
//        // this event gets dropped because its window is now closed :(
//    //    windowIn.pipeInput("A", "v5", 10L);
//
//    }

    @BeforeEach
    public void testSetup() {

        super.testSetup();
        testDriver = new TopologyTestDriver(kafkaStreamBuilder.getTopologyCount(), properties);

        windowIn = testDriver.createInputTopic(topic_in,
                Serdes.String().serializer(), Serdes.String().serializer());
        countOut = testDriver.createOutputTopic(topic_count, Serdes.String().deserializer(), Serdes.String().deserializer());

    }

    protected static List<String> getList() {
        List<String> list = new ArrayList<>();
        LocalDateTime localDateTime = LocalDateTime.now().withHour(12).withMinute(00).withSecond(0);
        final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        list.add("id01::ref01::8.2::" + sdf.format(localDateTime));
        list.add("id01::ref01::4.5::" + sdf.format(localDateTime.plusMinutes(5)));
        list.add("id01::ref01::5.3::" + sdf.format(localDateTime.plusMinutes(8)));
        list.add("id01::ref01::2.1::" + sdf.format(localDateTime.plusMinutes(11)));
        list.add("id01::ref01::8.3::" + sdf.format(localDateTime.plusMinutes(15)));
        list.add("id01::ref01::3.6::" + sdf.format(localDateTime.plusMinutes(18)));
        list.add("id01::ref01::4.4::" + sdf.format(localDateTime.plusMinutes(21)));
        list.add("id01::ref01::7.6::" + sdf.format(localDateTime.plusMinutes(25)));

        return list;

    }

}


