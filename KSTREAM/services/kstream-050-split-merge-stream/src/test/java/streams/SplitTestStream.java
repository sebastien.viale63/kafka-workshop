package streams;

import com.kafka.workshop.avro.InvalidWorkshopValue;
import com.kafka.workshop.avro.ValidWorkshopValue;
import com.kafka.workshop.avro.WorkshopValue;
import org.apache.kafka.streams.KeyValue;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SplitTestStream extends MockedKafkaClusterTestUtils{

    @Test
    public void splitTest() {
        
        String key = "globalKey-1";
        WorkshopValue workshopValue = WorkshopValue.newBuilder()
                .setId("globalId-1")
                .setReference("FROM GLOBAL 1")
                .setStat("globalStat-1")
                .setDescription("globalDesc-1")
                .setValid(true)
                .build();

        workshopValueTestInputTopic.pipeInput(key, workshopValue);

        key = "globalKey-2";
        workshopValue = WorkshopValue.newBuilder()
                .setId("globalId-2")
                .setReference("FROM GLOBAL 2")
                .setStat("globalStat-2")
                .setDescription("globalDesc-2")
                .setValid(false)
                .build();
        workshopValueTestInputTopic.pipeInput(key, workshopValue);


        key = "globalKey-3";
        workshopValue = WorkshopValue.newBuilder()
                .setId("globalId-3")
                .setReference("FROM GLOBAL 3")
                .setStat("globalStat-3")
                .setDescription("globalDesc-3")
                .setValid(true)
                .build();
        workshopValueTestInputTopic.pipeInput(key, workshopValue);


        key = getAlphaNumericString(3);
        ValidWorkshopValue validWorkshopValue = ValidWorkshopValue.newBuilder()
                .setId("invalidId-4")
                .setReference("invalidRef-4")
                .setStat("invalidStat-4")
                .setDescription("invalidDesc-4")
                .build();
        validWorkshopValueTestInputTopic.pipeInput(key, validWorkshopValue);


        List<KeyValue<String, ValidWorkshopValue>> validResults = validWorkshopValueTestOutputTopic.readKeyValuesToList();
        List<KeyValue<String, InvalidWorkshopValue>> invalidResults = invalidWorkshopValueTestOutputTopic.readKeyValuesToList();

        assertEquals(validResults.size(), 3);
        assertEquals(invalidResults.size(), 1);

    }

}
