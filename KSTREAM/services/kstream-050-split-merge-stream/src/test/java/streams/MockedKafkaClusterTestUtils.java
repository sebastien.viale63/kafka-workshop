package streams;

import com.kafka.workshop.avro.InvalidWorkshopValue;
import com.kafka.workshop.avro.ValidWorkshopValue;
import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.StreamExecutionContext;
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import solution.com.kafka.workshop.SplitMergeStream;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;


public class MockedKafkaClusterTestUtils {


    protected TopologyTestDriver testDriver;

    private final String topic_in = TopicsNames.SPLIT_STREAM_IN;
    private final String topic_valid_in = TopicsNames.SPLIT_VALID_STREAM_IN;
    private final String topic_valid_out = TopicsNames.SPLIT_VALID_STREAM_OUT;
    private final String topic_invalid_out = TopicsNames.SPLIT_INVALID_STREAM_OUT;
  
    TestInputTopic<String, WorkshopValue> workshopValueTestInputTopic;

    TestInputTopic<String, ValidWorkshopValue> validWorkshopValueTestInputTopic;
    
    TestOutputTopic<String, ValidWorkshopValue> validWorkshopValueTestOutputTopic;

    TestOutputTopic<String, InvalidWorkshopValue> invalidWorkshopValueTestOutputTopic;

    protected Map<String, String> serdeConfig;

    protected SplitMergeStream kafkaStreamBuilder;
    
    protected Properties properties;
    
    @BeforeEach
    public void testSetup() {
        kafkaStreamBuilder = new SplitMergeStream();
        // Create an instance of TopologyTestDriver with the topology to test
        properties = kafkaStreamBuilder.configureStream();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "test" + TopicsNames.PREFIX + "window-app-id");
        properties.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);

        this.serdeConfig = Collections.singletonMap("schema.registry.url", "mock://DUMMY_URL");
        StreamExecutionContext.setSerdesConfig(this.serdeConfig);

        testDriver = new TopologyTestDriver(kafkaStreamBuilder.getTopology(), properties);

        workshopValueTestInputTopic = testDriver.createInputTopic(topic_in,
                Serdes.String().serializer(), SerdesUtils.<WorkshopValue>getSerdes(false).serializer());
        validWorkshopValueTestInputTopic = testDriver.createInputTopic(topic_valid_in,
                Serdes.String().serializer(), SerdesUtils.<ValidWorkshopValue>getSerdes(false).serializer());
        validWorkshopValueTestOutputTopic = testDriver.createOutputTopic(topic_valid_out, 
                Serdes.String().deserializer(), SerdesUtils.<ValidWorkshopValue>getSerdes(false).deserializer());
        invalidWorkshopValueTestOutputTopic = testDriver.createOutputTopic(topic_invalid_out,
                Serdes.String().deserializer(), SerdesUtils.<InvalidWorkshopValue>getSerdes(false).deserializer());

    }

    @AfterEach
    public void close() {
        testDriver.close();

        FileUtils.deleteQuietly(new File(KafkaProperties.STORE_DIR));

        MockSchemaRegistry.dropScope(this.getClass().getName());
    }


    protected static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

}
