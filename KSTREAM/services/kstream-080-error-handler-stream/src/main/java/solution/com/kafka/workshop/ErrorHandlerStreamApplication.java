package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class ErrorHandlerStreamApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ErrorHandlerStreamApplication.class);
        application.run(args);
    }
	
}
