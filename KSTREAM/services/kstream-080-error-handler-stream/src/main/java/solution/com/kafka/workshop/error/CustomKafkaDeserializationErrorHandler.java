package solution.com.kafka.workshop.error;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomKafkaDeserializationErrorHandler extends LogAndContinueExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(CustomKafkaDeserializationErrorHandler.class);

    @Override
    public DeserializationHandlerResponse handle(final ProcessorContext context,
                                                 final ConsumerRecord<byte[], byte[]> record,
                                                 final Exception exception) {
         logger.info("Exception caught during Deserialization, " +
                        "taskId: {}, topic: {}, partition: {}, offset: {}, exception: {}",
                context.taskId(), record.topic(), record.partition(), record.offset(),
                exception);

        return DeserializationHandlerResponse.CONTINUE;
    }

}
