package solution.com.kafka.workshop;


import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.StreamExecutionContext;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Named;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import solution.com.kafka.workshop.error.CustomKafkaDeserializationErrorHandler;
import solution.com.kafka.workshop.error.CustomKafkaProductionErrorHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class ErrorHandlerStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(ErrorHandlerStream.class);

    private final String topic_stream_in = TopicsNames.ERROR_HANDLER_TOPIC_IN;
    private final String topic_out = TopicsNames.ERROR_HANDLER_TOPIC_OUT;

    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic_stream_in);
        Topics.createTopics(topic_out);

        try {
            // init serdes
            Map<String, String> serdesConfig = new HashMap<>();
            serdesConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG,
                    KafkaProperties.SCHEMA_REGISTRY);
            StreamExecutionContext.setSerdesConfig(serdesConfig);
            
            Topology topology = getTopology();
            logger.info("topology: {}", topology.describe());
            KafkaStreams streams = new KafkaStreams(topology, configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     *
     * @return
     */
    private Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "error-handler-app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // Set the replication factor for internal topics to 3
        streamsConfiguration.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 3);
        // Set the min in sync replicas for internal topics to 2
        streamsConfiguration.put(StreamsConfig.topicPrefix(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG), 2);
        // Set default tmp folder
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);
        // TODO Set default deserialization and default production exception handler
        streamsConfiguration.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, CustomKafkaDeserializationErrorHandler.class);
        streamsConfiguration.put(StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG, CustomKafkaProductionErrorHandler.class);

        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopology() {
        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, WorkshopStat> statStream = builder
                .stream(topic_stream_in, Consumed.with(Serdes.String(), SerdesUtils.<WorkshopStat>getSerdes(false)))
                .map((key, stat) -> {
                    if("99999999".equals(stat.getReference())){
                        char[] data = new char[9000000];
                        stat.setReference(new String(data));
                        return new KeyValue<String, WorkshopStat>("ko", stat);
                    }
                    return new KeyValue<String, WorkshopStat>(stat.getReference(), stat);
                        }, 
                        Named.as("ERROR_HANDLER_MAP_VALUES")); 
        statStream.to(topic_out, Produced.with(Serdes.String(), SerdesUtils.<WorkshopStat>getSerdes(false)));
        return builder.build();

    }
      
}

