package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.constants.KafkaProperties;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;


public class StreamProducer {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private static final String topic_stream_in = "handler-in-topic";
    
    public static void main(String[] args){
        StreamProducer streamProducer = new StreamProducer();
        streamProducer.run();
    }

    public void run() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopStat> producer = new KafkaProducer<>(props);

        try {

            String key;
            WorkshopStat workshopStat;

            for(int i=0; i<10; i++) {

                key = getAlphaNumericString(3);
                workshopStat = WorkshopStat.newBuilder()
                        .setId(i)
                        .setReference(getRandomReference())
                    //    .setReference("99999999")
                        .setStat(getRandomStat() + "::" + getRandomStat())
                        .build();

                ProducerRecord<String, WorkshopStat> record = new ProducerRecord<>(topic_stream_in, key, workshopStat);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }
          
    }

    public static String getRandomReference() {
        Random rand = new Random();
        List<String> givenList = new ArrayList<>();
        givenList.add("111111");
        givenList.add("222222");
        givenList.add("333333");
        givenList.add("444444");
        givenList.add("555555");
        givenList.add("666666");
        int randomIndex = rand.nextInt(givenList.size());
        String reference = givenList.get(randomIndex);
        return  reference;

    }

    private static String getRandomStat() {
        Random rand = new Random();
        List<String> givenList = new ArrayList<>();
        givenList.add("50");
        givenList.add("60");
        givenList.add("70");
        givenList.add("80");
        givenList.add("90");
        givenList.add("100");
        givenList.add("110");
        givenList.add("120");
        givenList.add("130");
        int randomIndex = rand.nextInt(givenList.size());
        String stat = givenList.get(randomIndex);
        return  stat;
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }
    

}
