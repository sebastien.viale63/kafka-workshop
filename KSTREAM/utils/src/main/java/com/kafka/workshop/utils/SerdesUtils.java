package com.kafka.workshop.utils;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.avro.specific.SpecificRecord;

public class SerdesUtils {

    public SerdesUtils() {
    }

    public static <T extends SpecificRecord> SpecificAvroSerde<T> getSerdes(boolean isKey) {
        SpecificAvroSerde<T> serde = new SpecificAvroSerde();
        serde.configure(StreamExecutionContext.getSerdesConfig(), isKey);
        return serde;
    }
}
